function start(){
    _doFile("res://src/Constants.nut");

    local winSize = Vec2(_window.getWidth(), _window.getHeight())
    _gui.setCanvasSize(winSize, winSize);

    _doFile("res://src/Base/Base.nut");

    ::Base.setup();
}

function update(){
    ::Base.update();
}

function end(){

}