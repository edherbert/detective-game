//TODO ideally this would be part of some namespace, i.e part of GameplayBase
::DialogManager <- {

    function setup(){
        _event.subscribe(EVENT.PROGRESS_DIALOG, eventReceiveProgressDialog, this);
    }

    function executeFile(filePath){
        _dialogSystem.compileAndRunDialog(filePath);
    }

    function eventReceiveProgressDialog(action, data){
        print("unblocking dialog");
        _dialogSystem.unblock();
    }


    //Called by the dialog script.
    function dialogString(dialog, actorId){
        _event.transmit(EVENT.DIALOG_NEW_TEXT, dialog);
    }

    function dialogEnded(){
        _event.transmit(EVENT.DIALOG_ENDED, null);
    }

    function dialogBegan(){
        _event.transmit(EVENT.DIALOG_BEGAN, null);
    }


};