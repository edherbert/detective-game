
::Base <- {
    "mCurrentState_": null,

    "mSetup_": false,

    "mRenderWindowWorkspace_": null,

    function setup(){
        //Setup the workspace which renders to the window.


        _doFile("res://src/Base/MainMenuBase.nut");
        _doFile("res://src/Base/GameplayBase.nut");

        _doFile("res://src/Gui/GuiScreenManager.nut");
        _doFile("res://src/Gui/Screen/MainMenuScreen.nut");
        _doFile("res://src/Gui/Screen/GameplayScreen.nut");
        _doFile("res://src/Gui/Screen/InventoryScreen.nut");

        startGameplay(null);
    }

    function update(){
        mCurrentState_.update();

        if(!mSetup_){
            mSetup_ = true;
            mRenderWindowWorkspace_ = _compositor.addWorkspace([_window.getRenderTexture()], _camera.getCamera(), "renderWindowWorkspace", true);
        }
    }

    function startMainMenu(){
        startState_(::MainMenuBase);
    }

    function startGameplay(saveState){
        startState_(::GameplayBase);
    }

    function startState_(stateClass){
        if(mCurrentState_) mCurrentState_.end();
        mCurrentState_ = stateClass();
        mCurrentState_.start();
    }
};