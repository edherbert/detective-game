::GameplayBase <- class{
    mRenderTexture_ = null;
    mRenderWorkspace_ = null;

    function start(){
        _doFile("res://src/Base/Gameplay/DialogManager.nut");
        ::DialogManager.setup();

        _camera.setPosition(0, 10, 20);
        _camera.lookAt(0, 0, 0);
        ::mesh <- _mesh.create("cube");

        //Render the scene to this texture
        mRenderTexture_ = _graphics.createTexture("gameplay/renderTexture");
        mRenderTexture_.setResolution(1920, 1080);
        mRenderTexture_.scheduleTransitionTo(_GPU_RESIDENCY_RESIDENT);
        mRenderWorkspace_ = _compositor.addWorkspace([mRenderTexture_], _camera.getCamera(), "renderTextureWorkspace", true);

        local datablockForWindow = _hlms.unlit.createDatablock("renderTextureDatablock");
        datablockForWindow.setTexture(0, "gameplay/renderTexture");

        ::GuiScreenManager.setCurrentScreen(::GuiScreenManager.GameplayScreen);
    }

    function update(){

    }

    function end(){
        //TODO destroy the workspace
    }
};