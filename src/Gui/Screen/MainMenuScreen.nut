::GuiScreenManager.MainMenuScreen <- class{

    mMainWindow_ = null;

    constructor(){
        mMainWindow_ = _gui.createWindow();
        mMainWindow_.setSize(_window.getWidth(), _window.getHeight());

        local layoutLine = _gui.createLayoutLine();

        local text = mMainWindow_.createLabel();
        text.setText("Who Killed Mr Burlington");
        layoutLine.addCell(text);

        local startGameButton = mMainWindow_.createButton();
        startGameButton.setDefaultFontSize(startGameButton.getDefaultFontSize() * 1.5);
        startGameButton.setText("Start game");
        startGameButton.attachListenerForEvent(function(widget, action){
            ::Base.startGameplay(null);
        }, 2, this);
        layoutLine.addCell(startGameButton);

        local settingsButton = mMainWindow_.createButton();
        settingsButton.setDefaultFontSize(settingsButton.getDefaultFontSize() * 1.5);
        settingsButton.setText("Settings");
        settingsButton.attachListenerForEvent(function(widget, action){
            print("Settings");
        }, 2, this);
        layoutLine.addCell(settingsButton);

        layoutLine.layout();
    }

    function end(){
        _gui.destroy(mMainWindow_);
    }

};