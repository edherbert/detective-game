::GuiScreenManager.InventoryScreen <- class{
    mWindow_ = null;

    constructor(){
        mWindow_ = _gui.createWindow();
        mWindow_.setSize(_window.getWidth(), _window.getHeight());

        local label = mWindow_.createLabel();
        label.setText("inventory screen");
    }

    function end(){
        _gui.destroy(mWindow_);
    }
};