::GuiScreenManager.GameplayScreen <- class{

    mWindow_ = null;
    mActionsSubWindow_ = null;

    mDialogSubWindow_ = null;
    mDialogSubWindowLabel_ = null;

    constructor(){
        mWindow_ = _gui.createWindow();
        mWindow_.setSize(_window.getWidth(), _window.getHeight());
        mWindow_.setClipBorders(0, 0, 0, 0);

        local gameplayPanel = mWindow_.createPanel();
        gameplayPanel.setSize(_window.getWidth(), _window.getHeight() * 0.9);
        gameplayPanel.setDatablock("renderTextureDatablock");


        setupActionsSubWindow_();
        setupDialogSubWindow_();

        showSubWindow(CURRENT_GUI_GAMEPLAY_SUBWINDOW.ACTIONS);
    }

    function end(){
        _gui.destroy(mWindow_);
    }

    function createSubWindow_(){
        local win = mWindow_.createWindow();
        win.setSize(_window.getWidth(), _window.getHeight() * 0.1);
        win.setPosition(0, _window.getHeight() * 0.9);
        win.setHidden(true);
        return win;
    }

    function setupDialogSubWindow_(){
        mDialogSubWindow_ = createSubWindow_();

        mDialogSubWindowLabel_ = mDialogSubWindow_.createLabel();
        mDialogSubWindowLabel_.setText(" ");

        local button = mDialogSubWindow_.createButton();
        button.setClipBorders(0, 30, 30, 0);
        button.setText("next");
        button.setSize(button.getSize().x, mDialogSubWindow_.getSizeAfterClipping().y);
        button.setPosition(mDialogSubWindow_.getSizeAfterClipping().x - button.getSize().x, 0);
        button.attachListenerForEvent(function(widget, action){
            _event.transmit(EVENT.PROGRESS_DIALOG, null);
        }, 2, this);
    }

    function setupActionsSubWindow_(){
        mActionsSubWindow_ = createSubWindow_();

        local layoutHoriz = _gui.createLayoutLine(_LAYOUT_HORIZONTAL);
        local buttons = ["Inspect", "Talk", "Interact", "Inventory"];
        local functions = [
            function(widget, action){
                print("inspecting");
            },
            function(widget, action){
                print("talking");
            },
            function(widget, action){
                print("interacting");
            },
            function(widget, action){
                print("inventory");

                //::DialogManager.executeFile("res://build/assets/dialog/test.dialog");
                ::GuiScreenManager.setCurrentScreen(::GuiScreenManager.InventoryScreen);
            },
        ];
        foreach(c,i in buttons){
            local button = mActionsSubWindow_.createButton();
            button.setClipBorders(0, 30, 30, 0);
            button.setText(i);
            button.setSize(button.getSize().x, mActionsSubWindow_.getSizeAfterClipping().y);
            button.attachListenerForEvent(functions[c], 2, this);
            layoutHoriz.addCell(button);
        }
        layoutHoriz.setMarginForAllCells(10, 10);
        layoutHoriz.setGridLocationForAllCells(0);
        layoutHoriz.layout();

        _event.subscribe(EVENT.DIALOG_BEGAN, receiveDialogBegan, this);
        _event.subscribe(EVENT.DIALOG_NEW_TEXT, receiveDialogNewText, this);
        _event.subscribe(EVENT.DIALOG_ENDED, receiveDialogEnded, this);
    }

    function showSubWindow(targetWindow){
        if(targetWindow == CURRENT_GUI_GAMEPLAY_SUBWINDOW.ACTIONS){
            mActionsSubWindow_.setHidden(false);
            mDialogSubWindow_.setHidden(true);

            mActionsSubWindow_.setConsumeCursor(true);
            mDialogSubWindow_.setConsumeCursor(false);
        }else{
            mActionsSubWindow_.setHidden(true);
            mDialogSubWindow_.setHidden(false);

            mActionsSubWindow_.setConsumeCursor(false);
            mDialogSubWindow_.setConsumeCursor(true);
        }
    }

    //-- Dialog callbacks
    function receiveDialogBegan(id, value){
        showSubWindow(CURRENT_GUI_GAMEPLAY_SUBWINDOW.DIALOG);
    }

    function receiveDialogNewText(id, value){
        mDialogSubWindowLabel_.setText(value);
    }

    function receiveDialogEnded(id, value){
        showSubWindow(CURRENT_GUI_GAMEPLAY_SUBWINDOW.ACTIONS);
    }
    //--

};